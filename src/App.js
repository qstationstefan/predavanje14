import { useCallback, useEffect, useState } from "react";
import Title from './components/Title';
import Button from './components/Button';

function App() {
  const [showTitle, setShowTitle] = useState(true);
  const [value, setValue] = useState(0);

  useEffect(() => {
    setInterval(() => {
      setValue(prevState => prevState + 1);
    }, 1000)
  }, [])

  const printValue = useCallback(() => {
    console.log("Value: ", value);
  }, [value])

  // const showTitleHandler = () => {
  //   setShowTitle(prevState => !prevState);
  // }

  const showTitleHandler = useCallback(() => {
    console.log("use callback funckija")
    setShowTitle(prevState => !prevState);
  }, []);

  console.log("Render app. Value: ", value);

  return (
    <div>
      <h1>Optimizacija</h1>
      <Button clickHandler={showTitleHandler}>Show/hide title</Button>
      <Button clickHandler={printValue}>Print value</Button>
      {/* <button onClick={showTitleHandler}>Show/hide title</button> */}
      {/* {showTitle && <p>Title</p>} */}
      <Title showTitle={showTitle} />
    </div>
  );
}

export default App;