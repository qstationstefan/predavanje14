import React from "react";

const Button = (props) => {
    console.log("Button render.")
    return (
        <button onClick={props.clickHandler}>{props.children}</button>
    )
}

export default React.memo(Button);