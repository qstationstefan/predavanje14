import React from "react";

const Title = (props) => {
    // console.log("Title render");
    return (<p>{props.showTitle ? "Title" : ""}</p>)
}

export default React.memo(Title);